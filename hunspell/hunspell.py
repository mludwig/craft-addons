import info


class subinfo(info.infoclass):
    def setTargets(self):
        for ver in ['1.3.4']:
            self.targets[ver] = 'https://github.com/hunspell/hunspell/archive/v%s.tar.gz' % ver
            self.targetInstSrc[ver] = 'hunspell-%s' % ver

        self.targetDigests['1.3.4'] = (['55918522cb0041748507dd4f5aa92d043cef337aa589b9367c2221da084281ac'], CraftHash.HashAlgorithm.SHA256)
        self.patchToApply['1.3.4'] = [('hunspell-1.3.4-add_cmakelists.diff', 1)]
        self.description = "Hunspell spell checker"
        self.defaultTarget = '1.3.4'

    def setDependencies(self):
        self.runtimeDependencies["virtual/base"] = "default"


from Package.CMakePackageBase import *


class Package(CMakePackageBase):
    def __init__(self, **args):
        CMakePackageBase.__init__(self)
