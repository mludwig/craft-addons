import info


class subinfo(info.infoclass):
    def setDependencies(self):
        self.runtimeDependencies["virtual/base"] = "default"

    def setTargets(self):
        for ver in ['5.0']:

            targetList = []
            baseUrl = "https://cgit.freedesktop.org/libreoffice/dictionaries/plain/"
            baseUrl = "https://github.com/LibreOffice/dictionaries/raw/master/"
            dictPrefix = ['af_ZA/af_ZA',
                          'an_ES/an_ES',
                          'ar/ar',
                          'be_BY/be_BY',
                          'bg_BG/bg_BG',
                          'bn_BD/bn_BD',
                          'bo/bo',
                          'br_FR/br_FR',
                          'bs_BA/bs_BA',
                          'ca/dictionaries/ca-valencia',
                          'ca/dictionaries/ca',
                          'cs_CZ/cs_CZ',
                          'da_DK/da_DK',
                          'de/de_AT_frami',
                          'de/de_CH_frami',
                          'de/de_DE_frami',
                          'el_GR/el_GR',
                          'en/en_AU',
                          'en/en_CA',
                          'en/en_GB',
                          'en/en_US',
                          'en/en_ZA',
                          'es/es_ANY',
                          'et_EE/et_EE',
                          'fr_FR/fr',
                          'gd_GB/gd_GB',
                          'gl/gl_ES',
                          'gug/gug',
                          'gu_IN/gu_IN',
                          'he_IL/he_IL',
                          'hi_IN/hi_IN',
                          'hr_HR/hr_HR',
                          'hu_HU/hu_HU',
                          'is/is',
                          'it_IT/it_IT',
                          'kmr_Latn/kmr_Latn',
                          'lo_LA/lo_LA',
                          'lt_LT/lt',
                          'lv_LV/lv_LV',
                          'ne_NP/ne_NP',
                          'nl_NL/nl_NL',
                          'no/nb_NO',
                          'no/nn_NO',
                          'oc_FR/oc_FR',
                          'pl_PL/pl_PL',
                          'pt_BR/pt_BR',
                          'pt_PT/pt_PT',
                          'ro/ro_RO',
                          'ru_RU/ru_RU',
                          'si_LK/si_LK',
                          'sk_SK/sk_SK',
                          'sl_SI/sl_SI',
                          'sr/sr-Latn',
                          'sr/sr',
                          'sv_SE/sv_FI',
                          'sv_SE/sv_SE',
                          'sw_TZ/sw_TZ',
                          'te_IN/te_IN',
                          'th_TH/th_TH',
                          'uk_UA/uk_UA',
                          'vi/vi_VN']

            for x in dictPrefix:
                targetList.append(baseUrl + x + ".aff")
                targetList.append(baseUrl + x + ".dic")

            self.targets[ver] = targetList
            #self.targetInstSrc[ver] = 'hunspell'
            self.targetInstallPath[ver] = os.path.join("bin", "data", "hunspell")
        #self.targetDigests['5.0'] = '49f274e67efdee771300cba4da1f3e4bc00be1ec'

        self.description = "Hunspell dictionaries"
        # note: use the version specified as DOCBOOKXML_CURRENTDTD_VERSION in
        #       FindDocBookXML.cmake of kdelibs or install all DTDs at the same time 
        self.defaultTarget = '5.0'

from Package.BinaryPackageBase import *


class Package(BinaryPackageBase):
    def __init__(self):
        BinaryPackageBase.__init__(self)
        self.subinfo.options.package.withCompiler = False
        self.subinfo.options.package.withSources = False

    def unpack(self):
        #print("workdir" + self.workDir())
        #utils.cleanDirectory(self.workDir())
        utils.createDir(self.sourceDir())
        print("source dir" + self.sourceDir())
        for name in self.localFileNames():
            src = os.path.join(CraftStandardDirs.downloadDir(), name)
            dst = os.path.join(self.sourceDir(), name)
            #print("src:" + src)
            #print("dst:" + dst)
            shutil.copy(src, dst)

        return True

    def install(self):
        self.cleanImage()
        utils.copyDir(self.sourceDir(), self.installDir(), linkOnly=False)

        return BuildSystemBase.install(self)
